# Chow Mein

## Ingredients

### Main

> Most of the ingredients are listed without measurements, the dish is very forgiving and try and match the amount of extra to the amount of noodles and meat

- Noodles
- 1 lb of Meat (Chicken, Pork, etc.)

### Optional

- Onion
- Garlic
- Celery
- Carrots
- Cabbage
- Scallions or Green Onions
- Garlic
- Ginger

### Sauce

- 2 Tbsp Oyster
- 2 Tbsp Soy Sauce
- 1 1/2 Tsp Rice Vinegar
- 1 1/2 Tsp Sugar
- 1 Tbsp Heat

## Directions

1. Cook noodles and set aside to dry
2. Sear meat
3. Stir Fry other ingredients
5. Fry noodles and garlic in Pan (more oil than you think)
6. Add meat and other ingredients
